#ifndef PC_H
# define PC_H

# include <stdint.h>

# include "dims.h"
# include "character.h"
# include "dungeon.h"

class object;
class pc : public character {
 public:
  int boss_kill_count;
  int32_t defence_bonus; 
  pc() 
  {
    boss_kill_count = 0;
    hp = 100; /* default hp value */
    damage = new dice(0,1,4);
    for (int i = 0; i < 10; i++)
      carry_slot[i] = NULL;
    for (int i = 0; i < 12; i++)
      equipment_slot[i] = NULL;
    defence_bonus = 0;
    speed = 10;
  }
  ~pc() {}
  object* equipment_slot[12];
  object* carry_slot[10];
  terrain_type_t known_terrain[DUNGEON_Y][DUNGEON_X];
  uint8_t visible[DUNGEON_Y][DUNGEON_X];
};

void pc_delete(pc *pc);
uint32_t pc_is_alive(dungeon *d);
void config_pc(dungeon *d);
uint32_t pc_next_pos(dungeon *d, pair_t dir);
void place_pc(dungeon *d);
uint32_t pc_in_room(dungeon *d, uint32_t room);
void pc_learn_terrain(pc *p, pair_t pos, terrain_type_t ter);
terrain_type_t pc_learned_terrain(pc *p, int16_t y, int16_t x);
void pc_init_known_terrain(pc *p);
void pc_observe_terrain(pc *p, dungeon *d);
int32_t is_illuminated(pc *p, int16_t y, int16_t x);
void pc_reset_visibility(pc *p);
void equip_object(pc *p, object *o);
void carry_object(pc *p, object *o);
int pick_up_object(pc *p, dungeon *d);
void wear_object(pc *p, int carry_slot_index);
int take_off_object(pc *p, int equipment_slot_index);
void drop_item(pc *p, dungeon *d, int carry_slot_index);
void expunge_item(pc *p, int carry_slot_index);
void attack_pc(pc *p, int32_t dam);
#endif
